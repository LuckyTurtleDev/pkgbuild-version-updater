#!/bin/bash
set -e
set -u


build()
{
	makepkg_args="--noarchive -cC"
	if [ "$nodeps" = "false" ]
	then
		makepkg_args+="s"
	else
		makepkg_args+="d"
	fi
	echo "	makepkg $makepkg_args"
	makepkg $makepkg_args
}

update()
{
	cd $1
	echo -e "\nupdate  $1"
	git checkout .
	git pull
	while read line
	do
		[[ $line =~ pkgver= ]] && declare "$line" && break
	done < PKGBUILD
	echo "$pkgver"
	test $(vercmp "$new_pkgver" "$pkgver" ) -lt 0 && echo "ERROR: The new pkgver $new_pkgver is lower as the current pkgver $pkgver." && exit 3
	test $(vercmp "$new_pkgver" "$pkgver" ) -eq 0 && echo "The new pkgver $new_pkgver and the current pkgver $pkgver are identical" && exit 2
	sed -i -r "s/pkgver=.*/pkgver=$new_pkgver/" PKGBUILD
	sed -i -r "s/pkgrel=.*/pkgrel=1/" PKGBUILD
	updpkgsums
	makepkg --printsrcinfo > .SRCINFO
	[[ "$nobuild" = "false" ]] && build
	cd ..
}

push()
{
	cd $1
	if [ "$noconfirm" = "false" ]
	then
		git diff
		echo "Press ENTER to push changes of $1"
		read
	fi
	git commit . -m "update to $new_pkgver"
	git push
	git clean -f
	cd ..
}

print_help()
{
	echo -e "USAGE:"
	echo -e "\tmakepkg-version-updater [OPTION] [NEW_PKGVER]"
	echo -e "OPTIONS:"
	echo -e "\t--nobuild  \tskip build"
	echo -e "\t--noconfirm\tdo not ask for confirmation before push"
	echo -e "\t--nodeps   \tdo not perform any dependency checks"
	echo -e "\t--pkgname  \tupdate the following AUR package(s) instead of updating the PKGBUILD(s) in the subdirectory(/ies) of the working directory"
	echo -e "EXITCODES:"
	echo -e "\t0\tsuccess"
	echo -e "\t1\terror parsing arguments"
	echo -e "\t2\tthe new pkgver and the current pkgver are identical"
	echo -e "\t3\tthe new pkgver is lower as the current pkgver"
	exit ${1:-0}
}



#parsing args:
[[ "$#" -eq 0 ]]                     && print_help 1
[[ "$1" = "-h" || "$1" = "--help" ]] && print_help
noconfirm="false"
nodeps="false"
nobuild="false"
package_mode="false"
package_names=()
arg_is_a_package_name="false"
for ((i = 1 ; i < $# ; i++))
do
	case "${!i}" in
		-h)          print_help;;
		--help)      print_help;;
		--noconfirm) arg_is_a_package_name="false"; noconfirm="true";;
		--nodeps)    arg_is_a_package_name="false"; nodeps="true";;
		--nobuild)   arg_is_a_package_name="false"; nobuild="true";;
		--pkgname)   package_mode="true"; arg_is_a_package_name="true";;
		*)           test "$arg_is_a_package_name" = "false"  && (echo "unknown option ${!i}"; print_help 1); package_names+=("${!i}");;
	esac
done
[[ "$package_mode" = "true" && ${#package_names[@]} -eq 0 ]] && echo "no package name(s) mention" && print_help 1;
new_pkgver=${!#}
[[ "$new_pkgver" =~ ^[0-9.]*r?[0-9]+$ ]] || ( echo "$new_pkgver is not an number" && print_help 1)

#update and push
if [ "$package_mode" = "false" ]
then
	for d in ./*/
	do
		update "$d"
	done
	for d in ./*/
	do
		push "$d"
	done
else
	working_path="/tmp/pkgbuild-version-updater"
	mkdir -p "$working_path"
	cd "$working_path"
	for pkg in "${package_names[@]}"
	do
		echo $pkg
		test -d "$working_path/$pkg" || git clone "ssh://aur@aur.archlinux.org/$pkg.git"
		update "$pkg"
	done
	for pkg in "${package_names[@]}"
	do
		push "$pkg"
	done
fi
